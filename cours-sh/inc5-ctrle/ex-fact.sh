# factorielle : $n! =$ si $n \leq 1$ alors $1$ sinon $n*(n-1)!$
if [ $1 -le 1 ]			# on suppose que l'argument 1 existe
then echo 1
else
    f=$(./fact $(($1-1)) )	# (n-1)!
    echo $(($1*f))
fi
