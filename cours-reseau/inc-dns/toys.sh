> dig +short 1234.words @dns.toys		# ou nslookup 1234.words dns.toys
"1234 = one thousand, two hundred thirty four"

> dig strasbourg/fr.weather @dns.toys
strasbourg. 1 IN TXT "Strasbourg (FR)" "3.20C" "66% hu." "clearsky_day" "11:00, Wed"
strasbourg. 1 IN TXT "Strasbourg (FR)" "5.70C" "59% hu." "clearsky_day" "13:00, Wed"
strasbourg. 1 IN TXT "Strasbourg (FR)" "7.10C" "55% hu." "clearsky_day" "15:00, Wed"

> nslookup -q=txt pi. dns.toys
pi      text = "3.141592653589793238462643383279502884197169"

> dig +short TXT 67400.cp.bortzmeyer.fr		# TXT ou LOC ou URI
"ILLKIRCH GRAFFENSTADEN"
