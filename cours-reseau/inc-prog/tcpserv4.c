main ()
{
  int s_cnx, s_dial, cli_len ;
  struct sockaddr_in serv_addr, cli_addr ;

  s_cnx = socket (PF_INET, SOCK_STREAM, 0) ;

  bzero ((char *) &serv_addr, sizeof serv_addr) ;
  serv_addr.sin_family = AF_INET ;
  serv_addr.sin_addr.s_addr = htonl (INADDR_ANY) ;
  serv_addr.sin_port = htons (5000) ;

  bind (s_cnx, &serv_addr, sizeof serv_addr) ;
  listen (s_cnx, 5) ;

  for (;;)
  {
    cli_len = sizeof cli_addr ;
    s_dial = accept (s_cnx, &cli_addr, &cli_len) ;
    serveur_tcp (s_dial) ;
    close (s_dial) ;
  }
}

void serveur_tcp (int sock)
{
    while (read (sock, ...) > 0)
    {
        // traiter l'envoi et préparer la réponse
        write (sock, ...) ;
    }
}
