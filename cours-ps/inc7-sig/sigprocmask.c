void incrementer (void) {
    sigset_t new, old ;

    // masquer SIGINT pour l'empêcher de perturber l'incrémentation
    sigemptyset (&new) ;
    sigaddset (&new, SIGINT) ;		    // new $\leftarrow$ \{ SIGINT \}
    sigprocmask (SIG_BLOCK, &new, &old) ;   // début de section critique

    if (c.grand == UINT32_MAX) {
	c.grand = 0 ;
	c.tresgrand++ ;
    }
    else c.grand++ ;

    // démasquer SIGINT en rétablissant le précédent masque de signaux
    sigprocmask (SIG_SETMASK, &old, NULL) ; // fin de section critique
}
