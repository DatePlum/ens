$ echo '\033[5;67Hhello\033[12;34Hworld'
# séquence «~ESC [ <ligne> ; <colonne> H~» : déplacer le curseur
# \implique affiche «~hello~» à la position <5,67> et «~world~» à la position <12,34>

# lit des caractères et les affiche en hexadécimal et en ASCII
$ read X ; echo "$X" | hd
# après appui sur les touches [\textuparrow] et [Entrée], on obtient l'affichage :
0000000  1b 5b 41 0a                     |.[A.|
# les codes envoyés par le terminal sont ESC [ A

$ tput clear ; tput cup 5 67 ; echo 'hello'
# «~cup~» : directive "cursor address" de terminfo (cf manuels de \code{terminfo} et \code{tput})
