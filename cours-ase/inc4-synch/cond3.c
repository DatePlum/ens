condition c ;
mutex m ;
file *f ;

void *thread_traite (void *arg) {
  lock (&m) ;
  if (file_vide(f)) {
     cwait (&c, &m) ;  // \alert{$\leftarrow$ ici}
  }
  d = extraire_file (f) ;
  unlock (&m) ;
  traiter (d) ;
}

void *thread_produit (void *arg) {
  d = lire_donnee (...) ;
  lock (&m) ;	// si besoin de section critique
  ajouter_file (f, d) ;
  csignal (&c) ;
  unlock (&m) ;
}
